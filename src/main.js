import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import 'bootstrap'; 
import 'bootstrap/dist/css/bootstrap.min.css';
import '@/assets/css/global.css';
import '@/assets/css/simple-lines-icons.css';
import axios from 'axios';
import VueLogger from 'vuejs-logger';
import * as VueGoogleMaps from "vue2-google-maps";



var VueTruncate = require('vue-truncate-filter')
Vue.use(VueTruncate)

/////////////////// LOGGER CONFIGURATION /////////////////////7

const options = {
  isEnabled: true,
  logLevel : process.env.VUE_APP_LOGGER_LEVEL,
  stringifyArguments : false,
  showLogLevel : true,
  showMethodName : true,
  separator: '|',
  showConsoleColors: true
};

Vue.use(VueLogger, options);


Vue.config.productionTip = false;

Vue.use(VueGoogleMaps, {
  load: {
    key: "AIzaSyBKWqctofnuiEysOq7c823mHSRgD6iRn_g",

    libraries: "places" // necessary for places input
  }
});


new Vue({
  router,
  store,
  created: function() {
      this.$store.dispatch('loadData');
      axios.interceptors.request.use((config) => {
          if(localStorage.getItem('token') != null){
            config.headers['Authorization'] = localStorage.getItem('token');
          }
          return config;
      }, (error) => {
          
          return Promise.reject(error);
      });
      axios.interceptors.response.use((response) => {

          return response;
      }, (error) => {
          
          return Promise.reject(error);
      });
  },
  methods: {
    
  },
  render: h => h(App)
}).$mount('#app')

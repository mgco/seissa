import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
// import VueAxios from 'vue-axios';

//Vue.use(Vuex)
Vue.use(Vuex, axios);

const api = process.env.VUE_APP_API;
const api_v2 = process.env.VUE_APP_API_V2;
const api_terms = api + process.env.VUE_APP_API_SETTINGS_TAC;
const api_login = process.env.VUE_APP_API_LOGIN;

// const api = "http://34.222.97.29/Seissa.Web/api/application/";
// const api_v2 = "http://34.222.97.29/Seissa.Web/api/application/v2/";
// const api_terms = api + "Setting?id=LegalLicense";
// const api_login = "http://34.222.97.29/Seissa.Web/oauth2/token";

var serialize = function (obj) {
	let str = [];
	for (let p in obj)
		if (obj.hasOwnProperty(p)) {
			str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
		}
	return str.join("&");
};

export default new Vuex.Store({
  state: {
    //GENERIC
    token: "",
    loading:true,
    message:{type:'', desc:''},
    confirmResponse: false,
    settingUp: 'done',
    currentPage: '',
    redirect:'',
    navigationMain: '',
    //BUSINESS
    user: {email: ''},
    inversor: {},
    articles: [],
    companies: [],
    groups: [],
    notifications: [],
    categories: [],
    branches: [],
    financialInstitutions: [],
    schedule: [],
    scheduleGraphics: [],
    schedulePercent: 0,
    scheduleAttendance: '',
    documentYearAndMonth: [],
    document: '',
    documents: [],
    comments: [],
    tac: '',
    filter: {
      companyId: '',
      groupId: '',
      catgoryId: '',
      files: {
        companyId: '',
        groupId: '',
        catgoryId: '',
        month:'',
        year: '',
      },
      events: {
        year: '',
        month: '',
        day: '',
        id: '',
        event:'',
      },
    },
    tempFilter: {
      companyId: '',
      groupId: '',
    }
  },
  mutations: {
    setLoading(state, loading){
      state.loading = loading;
    },
    setToken(state, token){
      state.token = token;
    },
    setNavigationMain(state, navigationMain){
      state.navigationMain = navigationMain;
    },
    setConfirmResponse(state, confirmResponse){
      state.confirmResponse = confirmResponse;
    },
    setSettingUp(state, settingUp){
      state.settingUp = settingUp;
    },
    _setCurrentPage(state, currentPage){
      state.currentPage = currentPage;
    },
    setMessage(state, message){
      state.message = message;
    },
    setRedirect(state, redirect){
      state.redirect = redirect;
    },
    setUser(state, user){
      state.user = user;
    },
    setInversor(state, inversor){
      state.inversor = inversor;
    },
    setArticles(state, articles){
      state.articles = articles;
    },
    setCompanies(state, companies){
      state.companies = companies;
    },
    setGroups(state, groups){
      state.groups = groups;
    },
    setNotifications(state, notifications){
      state.notifications = notifications;
    },
    setComments(state, comments){
      state.comments = comments;
    },
    setCategories(state, categories){
      state.categories = categories;
    },
    setBranches(state, branches){
      state.branches = branches;
    },
    setFinancialInstitution(state, financialInstitutions){
      state.financialInstitutions = financialInstitutions;
    },
    setSchedule(state, schedule){
      state.schedule = schedule;
    },
    setScheduleGraphics(state, scheduleGraphics){
      state.scheduleGraphics = scheduleGraphics;
    },
    setSchedulePercent(state, schedulePercent){
      state.schedulePercent = schedulePercent;
    },
    setScheduleKnowAttendance(state, scheduleAttendance){
      state.scheduleAttendance = scheduleAttendance;
    },
    setTac(state, tac){
      state.tac = tac;
    },
    setDocument(state, document){
      state.document = document;
    },
    setDocuments(state, documents){
      state.documents = documents;
    },
    setDocumentYearAndMonth(state, documentYearAndMonth){
      state.documentYearAndMonth = documentYearAndMonth;
    },
    setFilter(state, filter){
      state.filter = filter;
    },
    setTempFilter(state, temp){
      state.tempFilter = temp;
    },
    setFilterFiles(state, files){
      state.filter.files = files;
    },
    setFilterEvents(state, events){
      state.filter.events = events;
    },
    
    logout(state) {
			state.token = "";
		}
  },
  actions: {
    loadData({commit, dispatch}){
      commit('setSettingUp', 'starting');
      //generic services
      dispatch('getTac');
      dispatch('loadDataFromLocaStorage');
    },
    loadDataFromLocaStorage({commit, dispatch}){
      if(localStorage.getItem("token") != null){
        commit('setToken', localStorage.getItem("token"));
        commit('setUser', {email: localStorage.getItem("email")});
        axios.defaults.headers.common["Authorization"] = this.state.token;
        dispatch('getDataByModule', this.state.currentPage);
      }else{
        commit('setSettingUp', 'done');
      }
    },
    setCurrentPage({commit}, page){
      commit('_setCurrentPage', page);
    },
    setToken({commit}, token){
      commit('setToken', token);
    },
    closeLoading({commit}){
      commit('setSettingUp', 'done');
    },
    setRedirect({commit}, redirect){
      commit('setRedirect', redirect);
    },
    ///////////////////// LOADER DATA ORCHESTATOR BY MODULE //////////////
    getDataByModule({commit, dispatch}, module){
      commit('setSettingUp', 'starting');
      switch(module){
        case "dashboard":
            dispatch('getInversor',this.state.user.email).then(res => {
                dispatch('getCompanies',this.state.inversor.InvestorId)
              .then( data => {
                return dispatch('getGroups',this.state.inversor.InvestorId)
              })
              .then( data => {
                return dispatch('getNotifications',this.state.inversor.InvestorId)
              })
              .then( data => {
                commit('setSettingUp', 'done');
              })
              .catch(error => console.error(error));
              
            }, error => {
              console.log(error);
            });
            
        break;

        case "articles":
          dispatch('getArticles',this.state.filter)
            .then( data => {
             commit('setSettingUp', 'done');
           })
           .catch(error => console.error(error));
            
        break;

        case "dashboard-company":
             dispatch('getCategories',this.state.filter)
             .then( data => {
              return dispatch('getBranches',this.state.inversor.InvestorId)
             })
              .then( data => {
                commit('setSettingUp', 'done');
              })
              .catch(error => console.error(error));
          break;

        case "dashboard-dividends":
             dispatch('getFinancialInstitution',this.state.inversor.InvestorId)
              .then( data => {
                commit('setSettingUp', 'done');
              })
              .catch(error => console.error(error));
          break;

        case "documents-modal-categories":
            dispatch('getCategories',this.state.filter.files)
             .then( data => {
               commit('setSettingUp', 'done');
             })
             .catch(error => console.error(error));
         break;
        
         case "dashboard-files-detail":
            return dispatch('getDocuments',this.state.filter)
             .then( data => {
               commit('setSettingUp', 'done');
               return new Promise((resolve, reject) => {
                resolve(data);
               });
             })
             .catch(error => console.error(error));
         break;

         case "dashboard-category-files":
            return dispatch('getDocuments',this.state.filter)
             .then( data => {
               commit('setSettingUp', 'done');
               return new Promise((resolve, reject) => {
                resolve(data);
               });
             })
             .catch(error => console.error(error));
         break;

         case "documents-modal-categories-no-store":
           return dispatch('getCategoriesNoStore',this.state.filter.files)
         //  .then( data => {
         //   return dispatch('getBranches',this.state.inversor.InvestorId)
         //  })
           .then( data => {
             commit('setSettingUp', 'done');
             return new Promise((resolve, reject) => {
              resolve(data);
             })
           })
           .catch(error => console.error(error));
       break;

        case "dashboard-agenda":
            return dispatch('getSchedule',this.state.filter.companyId)
             .then( data => {
               commit('setSettingUp', 'done');
               return new Promise((resolve, reject) => {
                resolve(data);
               })
             })
             .catch(error => console.error(error));
         break;
         
         case "getYearAndMonthsDocuments":
            return dispatch('getYearAndMonthsDocuments', this.state.filter)
             .then( data => {
               commit('setSettingUp', 'done');
               return new Promise((resolve, reject) => {
                resolve(data);
               })
             })
             .catch(error => console.error(error));
         break;
      }
      
    },
    ////////////////////////////// USERS /////////////////////////////////
    login({commit, dispatch}, user){
      let user_data = serialize({
        username: user.email,
        password: user.password,
        grant_type: "password"
      });
      return new Promise((resolve, reject) => {
        axios.post(api_login, user_data)
          .then((response)  =>  {
            if(response.data.hasOwnProperty("access_token")){
              localStorage.setItem("token", "Bearer " + response.data.access_token);
              localStorage.setItem("email", user.email);
              let _user = {email: user.email};
              commit('setUser', _user);
              commit('setToken', response.data.access_token);
              resolve(response);
            }
          }, (error)  =>  {
            localStorage.removeItem("token");
            let response = 'Ha ocurrido un error, intente nuevamente.'
            if(error.response.status == 400){
              commit('setMessage', {type:'ERROR',desc: error.response.data.error_description});
            }
            reject(error.response);
          })
          .catch(error => {
						reject(error);
					});;
        });
      
    },

    setBill({commit, dispatch}, form){
      commit('setSettingUp', 'starting');
      return new Promise((resolve, reject) => {
        axios.post(api + "Bill", form)
          .then((response)  =>  {
           commit('setSettingUp', 'done');
           commit('setMessage', {type:'OK',desc: "Solicitud enviada con éxito"});
           resolve(response);
          }, (error)  =>  {
            commit('setSettingUp', 'done');
            commit('setMessage', {type:'ERROR',desc: "Ha ocurrido un error, favor contactarse con el administrador del sistema"});
            reject(error.response);
          })
          .catch(error => {
            commit('setMessage', {type:'ERROR',desc: "Ha ocurrido un error, favor contactarse con el administrador del sistema(2)"});
            commit('setSettingUp', 'done');
						reject(error);
					});;
        });
      
    },

    setComment({commit, dispatch}, comment){
      commit('setSettingUp', 'starting');
      return new Promise((resolve, reject) => {
        axios.post(api + "Comment", comment)
          .then((response)  =>  {
           commit('setSettingUp', 'done');
           commit('setMessage', {type:'OK',desc: "Comentario agregado con éxito"});
           resolve(response);
          }, (error)  =>  {
            commit('setSettingUp', 'done');
            commit('setMessage', {type:'ERROR',desc: "Ha ocurrido un error, favor contactarse con el administrador del sistema"});
            reject(error.response);
          })
          .catch(error => {
            commit('setMessage', {type:'ERROR',desc: "Ha ocurrido un error, favor contactarse con el administrador del sistema(2)"});
            commit('setSettingUp', 'done');
						reject(error);
					});;
        });
      
    },

    setAssistance({commit, dispatch}, assistance){
      commit('setSettingUp', 'starting');
      return new Promise((resolve, reject) => {
        var url = "?AttendanceStateId="+assistance.AttendanceStateId;
        url += "&ScheduleId="+assistance.ScheduleId;
        axios.post(api_v2 + "UpdateAttendance"+url, assistance)
          .then((response)  =>  {
           commit('setSettingUp', 'done');
           commit('setMessage', {type:'OK',desc: response.data.Message});
           resolve(response);
          }, (error)  =>  {
            commit('setSettingUp', 'done');
            commit('setMessage', {type:'ERROR',desc: "Ha ocurrido un error, favor contactarse con el administrador del sistema"});
            reject(error.response);
          })
          .catch(error => {
            commit('setMessage', {type:'ERROR',desc: "Ha ocurrido un error, favor contactarse con el administrador del sistema(2)"});
            commit('setSettingUp', 'done');
						reject(error);
					});;
        });
      
    },

    setConfirmAttendance({commit, dispatch}, code){
      commit('setSettingUp', 'starting');
      return new Promise((resolve, reject) => {
        var url = "?code="+code.code;
        axios.post(api_v2 + "ConfirmAttendance"+url, code)
          .then((response)  =>  {
           commit('setSettingUp', 'done');
           if(response.data.Success){
            commit('setMessage', {type:'OK',desc: response.data.Message}); 
           }else{
            commit('setMessage', {type:'ERROR',desc: response.data.Message});
           }
            
           resolve(response);
          }, (error)  =>  {
            commit('setSettingUp', 'done');
            commit('setMessage', {type:'ERROR',desc: "Ha ocurrido un error, favor contactarse con el administrador del sistema"});
            reject(error.response);
          })
          .catch(error => {
            commit('setMessage', {type:'ERROR',desc: "Ha ocurrido un error, favor contactarse con el administrador del sistema(2)"});
            commit('setSettingUp', 'done');
						reject(error);
					});;
        });
      
    },
    
    setForgotPassword({commit, dispatch}, email){
      commit('setSettingUp', 'starting');
      return new Promise((resolve, reject) => {
        email = {email: email};
        axios.post(api_v2 + "ForgotPassword", email)
          .then((response)  =>  {
           commit('setSettingUp', 'done');
           if(response.data.Success){
              commit('setMessage', {type:'OK',desc: response.data.Message});
           }else{
            commit('setMessage', {type:'ERROR',desc: response.data.Message});
           }
           
           resolve(response);
          }, (error)  =>  {
            commit('setSettingUp', 'done');
            commit('setMessage', {type:'ERROR',desc: "Ha ocurrido un error, favor contactarse con el administrador del sistema"});
            reject(error.response);
          })
          .catch(error => {
            commit('setMessage', {type:'ERROR',desc: "Ha ocurrido un error, favor contactarse con el administrador del sistema(2)"});
            commit('setSettingUp', 'done');
						reject(error);
					});;
        });
      
    },
 
    _logout({commit}){
      delete axios.defaults.headers.common["Authorization"];
      localStorage.removeItem("token");
      localStorage.removeItem("email");
      commit('setUser', {email: ''});
      commit('setToken', '');
      commit('setInversor', {});
      commit('setArticles', []);
    },

    logout({commit, dispatch}) {
			return new Promise((resolve, reject) => {
        commit("logout");
        commit('setConfirmResponse', false);
        dispatch('_logout');
				resolve();
			});
    },
    confirm({commit}, message){
      commit('setMessage', message);
    },

    checkError({commit, dispatch}, error){
      console.log('checkError ' + JSON.stringify(error))
      if(error.message.indexOf('401') != -1){
        dispatch('_logout');
        window.location.href = window.location.href;
      }
    },

    ////////////////////////////// GETTERS ///////////////////////////////

    getTac({commit}){
      axios.get(api_terms)
        .then((response)  =>  {
          commit('setTac', response.data);
          commit('setLoading', false);
        }, (error)  =>  {
          this.loading = false;
        })
    },

    getInversor({commit, dispatch}, email){
      return new Promise((resolve, reject) => {
        axios.get(api_v2 + "Investor")
          .then((response)  =>  {
            commit('setInversor', response.data);
            resolve(response);
          })
          .catch(error => {
            dispatch('checkError', error);
						reject(error);
					});;
        });
    },

    getArticles({commit, dispatch}, id){
      return new Promise((resolve, reject) => {
        axios.get(api_v2 + "Article")
          .then((response)  =>  {
            commit('setArticles', response.data);
            resolve(response);
          })
          .catch(error => {
            dispatch('checkError', error);
						reject(error);
					});;
        });
    },

    getArticle({commit, dispatch}, uid){

      return new Promise((resolve, reject) => {
        axios.get(api_v2 + "FindArticle?uid="+ uid)
          .then((response)  =>  {
            resolve(response);
          })
          .catch(error => {
            dispatch('checkError', error);
						reject(error);
					});;
        });
    },

    getCompanies({commit, dispatch}, id){
      return new Promise((resolve, reject) => {
        axios.get(api_v2 + "Company")
          .then((response)  =>  {
            commit('setCompanies', response.data);
            resolve(response);
          })
          .catch(error => {
            dispatch('checkError', error);
						reject(error);
					});;
        });
    },

    getGroups({commit, dispatch}, id){
      return new Promise((resolve, reject) => {
        axios.get(api + "Group?id=" + id)
          .then((response)  =>  {
            commit('setGroups', response.data);
            resolve(response);
          })
          .catch(error => {
            dispatch('checkError', error);
						reject(error);
					});;
        });
    },

    getNotifications({commit, dispatch}, id){
      return new Promise((resolve, reject) => {
        axios.get(api_v2 + "Notification?id=" + id)
          .then((response)  =>  {
            commit('setNotifications', response.data);
            resolve(response);
          })
          .catch(error => {
            dispatch('checkError', error);
						reject(error);
					});;
        });
    },
    
    getComments({commit, dispatch}, id){
      return new Promise((resolve, reject) => {
        axios.get(api + "comment?id=" + id)
          .then((response)  =>  {
            commit('setComments', response.data);
            resolve(response);
          })
          .catch(error => {
            dispatch('checkError', error);
						reject(error);
					});;
        });
    },

    getCommentsV2({commit, dispatch}, id){
      return new Promise((resolve, reject) => {
        axios.get(api_v2 + "Comment?documentId=" + id)
          .then((response)  =>  {
            commit('setComments', response.data);
            resolve(response);
          })
          .catch(error => {
            dispatch('checkError', error);
						reject(error);
					});;
        });
    },

    getCategories({commit, dispatch}, filter){
      let groupId = filter.groupId;
      let companyId = filter.companyId;
      let url = "?groupId="+groupId+"&companyId="+companyId;
      return new Promise((resolve, reject) => {
        axios.get(api_v2 + "Group/Categories"+url)
          .then((response)  =>  {
            commit('setCategories', response.data);
            resolve(response);
          })
          .catch(error => {
            dispatch('checkError', error);
						reject(error);
					});;
        });
    },

    getCategoriesNoStore({commit, dispatch}, filter){
      let groupId = filter.groupId;
      let companyId = filter.companyId;
      let url = "?groupId="+groupId+"&companyId="+companyId;
      return new Promise((resolve, reject) => {
        axios.get(api_v2 + "Group/Categories"+url)
          .then((response)  =>  {
            resolve(response.data);
          })
          .catch(error => {
            dispatch('checkError', error);
						reject(error);
					});;
        });
    },


    getBranches({commit, dispatch}, id){
      return new Promise((resolve, reject) => {
        axios.get(api + "Branch")
          .then((response)  =>  {
            commit('setBranches', response.data);
            resolve(response);
          })
          .catch(error => {
            dispatch('checkError', error);
						reject(error);
					});;
        });
    },

    getFinancialInstitution({commit, dispatch}, id){
      return new Promise((resolve, reject) => {
        axios.get(api + "FinancialInstitution")
          .then((response)  =>  {
            commit('setFinancialInstitution', response.data);
            resolve(response);
          })
          .catch(error => {
            dispatch('checkError', error);
						reject(error);
					});;
        });
    },
    
    
    getSchedule({commit, dispatch}, id){
      return new Promise((resolve, reject) => {
        axios.get(api_v2 + "Group/Schedules?companyId="+id)
          .then((response)  =>  {
            commit('setSchedule', response.data);
            resolve(response);
          })
          .catch(error => {
            dispatch('checkError', error);
						reject(error);
					});
        });
    },

    getScheduleGraphics({commit, dispatch}, url){
      return new Promise((resolve, reject) => {
        axios.get(api_v2 + "DataGraphics?"+url)
          .then((response)  =>  {
            commit('setScheduleGraphics', response.data);
            commit('setSchedulePercent', response.data.Percentage);
            resolve(response);
          })
          .catch(error => {
            dispatch('checkError', error);
						reject(error);
					});
        });
    },

    getScheduleKnowAttendance({commit, dispatch}, id){
      return new Promise((resolve, reject) => {
        axios.get(api_v2 + "KnowAttendance?scheduleId="+id)
          .then((response)  =>  {
            commit('setScheduleKnowAttendance', response.data);
            resolve(response);
          })
          .catch(error => {
            dispatch('checkError', error);
						reject(error);
					});
        });
    },




    getYearAndMonthsDocuments({commit, dispatch}, filter){
      return new Promise((resolve, reject) => {
        axios.get(api_v2 + "Documents/Date?companyId="+filter.companyId
                            +"&groupId="+filter.groupId
                            +"&categoryId="+filter.categoryId)
          .then((response)  =>  {
            commit('setDocumentYearAndMonth', response.data);
            resolve(response);
          })
          .catch(error => {
            dispatch('checkError', error);
						reject(error);
					});;
        });
    },
    
    getDocuments({commit, dispatch}, filter){
      return new Promise((resolve, reject) => {
        axios.get(api_v2 + "Group/Documents?companyId="+filter.companyId
                            +"&groupId="+filter.groupId
                            +"&year="+filter.year
                            +"&month="+filter.month)
          .then((response)  =>  {
            commit('setDocuments', response.data);
            resolve(response);
          })
          .catch(error => {
            dispatch('checkError', error);
						reject(error);
					});;
        });
    },
 
    getDocument({commit, dispatch}, url){
      return new Promise((resolve, reject) => {
        axios.get(url)
          .then((response)  =>  {
            resolve(response);
          })
          .catch(error => {
            dispatch('checkError', error);
						reject(error);
					});;
        });
    },

    getDocumentV2({commit, dispatch}, uid){
      return new Promise((resolve, reject) => {
        axios.get(api_v2 + "FindDocument?uid="+uid)
          .then((response)  =>  {
            resolve(response);
          })
          .catch(error => {
            dispatch('checkError', error);
						reject(error);
					});;
        });
    },
    
    getImage({commit, dispatch}, url){
      return new Promise((resolve, reject) => {
        axios.get(url)
          .then((response)  =>  {
            resolve(response);
          })
          .catch(error => {
            dispatch('checkError', error);
						reject(error);
					});;
        });
    },

    ///////////////////////////// POST //////////////////

    
  },

  getters: {

    groupsByCompany: state => companyId => {
			return state.groups.filter(c => c.CompanyId == companyId);
    },    

    branchesByCompany: state => companyId => {
			if (state.branches != null && state.branches != undefined) {
				return state.branches.filter(c => c.CompanyId == companyId);
			}
			return [];
    },
    
    currentFinancialInstitutions: state => {
			return state.financialInstitutions;
		},
    
    commentsByDocument: state => doc => {
			if (doc != null && doc != undefined) {
				if (state.comments) {
					return state.comments.filter(
						c => c.DocumentId == doc.DocumentId
					);
				}
			}
			return [];
		},
  }
})

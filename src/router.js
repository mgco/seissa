import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import store from './store';

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/about',
      name: 'about',
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    },
    {
      path: '/list',
      name: 'list',
      component: () => import('./views/ListRoutes.vue')
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('./views/general/Login.vue')
    },
    {
      path: '/profile',
      name: 'profile',
      component: () => import('./views/general/Profile.vue')
    },
    {
      path: '/password/reset',
      name: 'password-reset',
      component: () => import('./views/general/PasswordReset.vue')
    },
    {
      path: '/term-and-conditions',
      name: 'term-and-conditions',
      component: () => import('./views/general/TermsAndConditions.vue')
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: () => import(/* webpackChunkName: "dashboard" */'./views/dashboard/Dashboard.vue')
    },
    {
      path: '/dashboard/files',
      name: 'dashboard-files',
      component: () => import('./views/dashboard/Files.vue')
    },
    {
      path: '/dashboard/files/month',
      name: 'dashboard-files-month',
      component: () => import('./views/dashboard/FilesMonth.vue')
    },
    {
      path: '/dashboard/files/detail',
      name: 'dashboard-files-detail',
      component: () => import('./views/dashboard/FilesDetail.vue')
    },
    {
      path: '/dashboard/category/files',
      name: 'dashboard-category-files',
      component: () => import('./views/dashboard/FilesDetailWithOutMonthAndYear.vue')
    },
    {
      path: '/dashboard/company',
      name: 'dashboard-company',
      component: () => import('./views/dashboard/Company.vue')
    },
    {
      path: '/dashboard/file',
      name: 'dashboard-file',
      component: () => import('./views/dashboard/File.vue')
    },
    {
      path: '/dashboard/comments',
      name: 'dashboard-comments',
      component: () => import('./views/dashboard/Comments.vue')
    },
    {
      path: '/dashboard/dividends',
      name: 'dashboard-dividends',
      component: () => import('./views/dashboard/Dividends.vue')
    },
    {
      path: '/dashboard/venues/list',
      name: 'dashboard-venues-list',
      component: () => import('./views/dashboard/VenuesList.vue')
    },
    {
      path: '/dashboard/venues/maps',
      name: 'dashboard-venues-maps',
      component: () => import('./views/dashboard/VenuesMap.vue')
    },
    {
      path: '/dashboard/news',
      name: 'dashboard-news',
      component: () => import('./views/dashboard/News.vue')
    },
    {
      path: '/dashboard/agenda',
      name: 'dashboard-agenda',
      component: () => import('./views/dashboard/Agenda.vue')
    },
    {
      path: '/dashboard/agendaDetails',
      name: 'dashboard-agenda-details',
      component: () => import('./views/dashboard/AgendaDetails.vue')
    },
    {
      path: '/dashboard/agendaConfirm',
      name: 'dashboard-agenda-confirm',
      component: () => import('./views/dashboard/AgendaConfirm.vue')
    },
    {
      path: '/dashboard/news/:id',
      name: 'dashboard-news-item',
      component: () => import('./views/dashboard/NewsItem.vue')
    },
    {
      path: '/dashboard/notifications',
      name: 'dashboard-notifications',
      component: () => import('./views/dashboard/Notifications.vue')
    },
    {
      path: '/dashboard/notifications/1',
      name: 'dashboard-notifications-item',
      component: () => import('./views/dashboard/NotificationsItem.vue')
    },
    {
      path: '/go/new/:id',
      name: 'go-item',
      component: () => import('./views/dashboard/GoNew.vue')
    },
    {
      path: '/go/document/:id',
      name: 'go-document',
      component: () => import('./views/dashboard/GoDocument.vue')
    },
    {
      path: '/go/categories/:id',
      name: 'go-categories',
      component: () => import('./views/dashboard/GoCategories.vue')
    },
    {
      path: '/go/category/:id/:id2/:id3',
      name: 'go-category',
      component: () => import('./views/dashboard/GoCategory.vue')
    },

    // otherwise redirect to home
    { path: '*', redirect: '/' }
  ]
})


router.beforeEach((to, from, next) => {
  // redirect to login page if not logged in and trying to access a restricted page
  const publicPages = ['/','/login', '/term-and-conditions', '/password/reset'];
  const authRequired = !publicPages.includes(to.path);
  store.dispatch('setCurrentPage', to.name);
  
  if(process.env.VUE_APP_HTTPS == "true"){
    var urlOrigin = window.location.href;
    if(urlOrigin.indexOf('http://') != -1){
      urlOrigin = urlOrigin.replace('http://', 'https://');
      window.location.href = urlOrigin;
    }
  
  }
  localStorage.setItem("lastlogin", "here");
  if (authRequired && store.state.token == "") {
    if(localStorage.getItem("token") != null){
      //cuando se recarga la pagina redieccionamos al dashboard
      store.dispatch('setToken', localStorage.getItem("token"));
      if(to.path.indexOf('/dashboard/news/') != -1){
        return next(to.path)
      }
      if(to.path.indexOf('/go/new/') != -1 
        || to.path.indexOf('/go/document/') != -1 
        || to.path.indexOf('/go/categories/') != -1
        || to.path.indexOf('/go/category/') != -1){
        return next(to.path)
      }
      return next('/dashboard');
    }else{
      if(to.path != null && to.path.indexOf('/go/new/') != -1){
        store.dispatch('setRedirect', to.path);
      }
      if(to.path != null && to.path.indexOf('/go/document/') != -1){
        store.dispatch('setRedirect', to.path);
      }
      if(to.path != null && to.path.indexOf('/go/categories/') != -1){
        store.dispatch('setRedirect', to.path);
      }
      if(to.path != null && to.path.indexOf('/go/category/') != -1){
        store.dispatch('setRedirect', to.path);
      }
    }
    return next('/login');
  }
  next();
})

export default router; 